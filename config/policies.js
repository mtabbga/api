/**
 * Policy Mappings
 * (sails.config.policies)
 *
 * Policies are simple functions which run **before** your actions.
 *
 * For more information on configuring policies, check out:
 * https://sailsjs.com/docs/concepts/policies
 */

module.exports.policies = {

  /***************************************************************************
  *                                                                          *
  * Default policy for all controllers and actions, unless overridden.       *
  * (`true` allows public access)                                            *
  *                                                                          *
  ***************************************************************************/

  // '*': true,
  // socket
  'channel/join': ['isTwitchChannel'],
  'channel/leave': ['isTwitchChannel'],
  'viewer/join': ['isTwitchViewer'],
  'viewer/leave': ['isTwitchViewer'],
  'play/start': ['isTwitchChannel'],
  'play/predict': ['isTwitchViewer'],
  'play/stop': ['isTwitchChannel']
}
