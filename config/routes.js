/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {
  // Channel IO
  'get /streamer/join': 'channel.join',
  'get /streamer/leave': 'channel.leave',
  // Viewer IO
  'get /viewer/join/:channel': 'viewer.join',
  'get /viewer/leave/:channel': 'viewer.leave',
  // Ops
  'post /streamer/start': 'play.start',
  'post /streamer/stop': 'play.stop',
  'post /viewer/predict': 'play.predict',
  // Socre
  'get /channel/:channel/score/:viewer': 'ChannelController.score',
  'get /channel/:channel/top10': 'ChannelController.topTen'

}
