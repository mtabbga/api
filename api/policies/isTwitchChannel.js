module.exports = async (req, res, proceed) => {
  //console.log(req.headers)
  const id = req.headers.userid
  if (!id) {
    return res.unauthorized(new Error('No twitch user was provided'))
  }
  try {
    let user = await Channel.findOne({ id })
    // do something at twitch to fetch the user
    if (!user) {
      user = await Channel.create({ id }).fetch()
    }
    req.user = user.id
    return proceed()
  } catch (e) {
    return res.serverError(e)
  }
}
