module.exports = {
  friendlyName: 'Join',
  description: 'Join a channel.',
  inputs: {},
  exits: {
    serverError: {
      responseType: 'serverError'
    },
    badRequest: {
      responseType: 'badRequest'
    },
    noContent: {
      responseType: 'noContent'
    }
  },
  fn: async function (inputs, exits) {
    if (!this.req.isSocket) {
      return exits.badRequest(new Error('This is reserved to socket only'))
    }
    sails.sockets.join(this.req, this.req.user)
    sails.sockets.broadcast(this.req.user, 'streamerJoined', { channel: this.req.user }, this.req)
    return exits.noContent()
  }
}
