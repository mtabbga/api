module.exports = {
  friendlyName: 'Leave',
  description: 'Leave a channel.',
  inputs: {},
  exits: {
    serverError: {
      responseType: 'serverError'
    },
    badRequest: {
      responseType: 'badRequest'
    },
    noContent: {
      responseType: 'noContent'
    }
  },
  fn: async function (inputs, exits) {
    if (!this.req.isSocket) {
      return exits.badRequest(new Error('This is reserved to socket only'))
    }
    sails.sockets.broadcast(this.req.user, 'streamerLeft', { channel: this.req.user }, this.req)
    sails.sockets.leaveAll(this.req.user)
    return exits.noContent()
  }
}
