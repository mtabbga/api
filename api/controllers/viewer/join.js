module.exports = {
  friendlyName: 'Join',
  description: 'Join a channel.',
  inputs: {
    channel: {
      type: 'string',
      description: 'The streamer Id.',
      required: true
    }
  },
  exits: {
    serverError: {
      responseType: 'serverError'
    },
    badRequest: {
      responseType: 'badRequest'
    },
    notFound: {
      responseType: 'notFound'
    },
    success: {
      responseType: 'ok'
    }
  },
  fn: async function (inputs, exits) {
    if (!this.req.isSocket) {
      return exits.badRequest(new Error('This is reserved to socket only'))
    }
    try {
      const channel = await Channel.findOne({ id: inputs.channel })
      if (!channel) {
        return exits.notFound(new Error('trying to join a non existant or non persisted channel.'))
      }
      const score = await Prediction.score({ viewer: this.req.user, channel: channel.id })
      const topTen = await Prediction.topTen(channel.id)
      sails.sockets.join(this.req, channel.id)
      sails.sockets.broadcast(channel.id, 'viewerJoined', {viewer: this.req.user}, this.req)
      return exits.success({score, topTen})
    } catch (e) {
      return exits.serverError(e)
    }
  }
}
