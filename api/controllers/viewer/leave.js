module.exports = {
  friendlyName: 'Leave',
  description: 'Leave a channel.',
  inputs: {
    channel: {
      type: 'string',
      description: 'The streamer Id.',
      required: true
    }
  },
  exits: {
    serverError: {
      responseType: 'serverError'
    },
    badRequest: {
      responseType: 'badRequest'
    },
    success: {
      responseType: 'ok'
    }
  },
  fn: async function (inputs, exits) {
    if (!this.req.isSocket) {
      return exits.badRequest(new Error('This is reserved to socket only'))
    }
    sails.sockets.broadcast(inputs.channel, 'viewerLeft', {viewer: this.req.user}, this.req)
    sails.sockets.leave(inputs.channel)
    return exits.success({ channel: inputs.channel })
  }
}
