module.exports = {
  friendlyName: 'Predict',
  description: 'Predict a play.',
  inputs: {
    play: {
      type: 'string',
      description: 'The current active play id to predict on.',
      required: false
    },
    prediction: {
      type: ['string'],
      description: 'Card moves guess by viewers.',
      required: true
    }
  },
  exits: {
    serverError: {
      responseType: 'serverError'
    },
    badRequest: {
      responseType: 'badRequest'
    },
    notFound: {
      responseType: 'notFound'
    },
    gone: {
      responseType: 'gone'
    },
    success: {
      responseType: 'ok'
    }
  },
  fn: async function (inputs, exits) {
    if (!inputs.channel && !inputs.play) {
      return exits.badRequest(new Error('Viewer need to provide either channel or play'))
    }
    if (!this.req.isSocket) {
      return exits.badRequest(new Error('This is reserved to socket only'))
    }
    try {
      const play = await Play.findOne({id: inputs.play})
      if (!play) {
        return exits.notFound(new Error('Can not find the play that the viewer is trying to predict'))
      }
      if (play.status !== 'in progress') {
        return exits.gone(new Error('The play that the steamer is trying to stop has expired or finished'))
      }
      const prediction = await Prediction.create({
        attempt: inputs.prediction,
        status: 'in progress',
        channel: play.channel,
        play: play.id,
        viewer: this.req.user
      }).fetch()
      const hiddenPrediction = _.omit(prediction, ['attempt'])
      sails.sockets.broadcast(play.channel, 'prediction', {hiddenPrediction}, this.req)
      return exits.success(prediction)
    } catch (e) {
      return exits.serverError(e)
    }
  }
}
