module.exports = {
  friendlyName: 'Start',
  description: 'Start a prediction play.',
  inputs: {
    hand: {
      type: ['string'],
      description: 'The deck hand at the start of the play',
      required: true
    }
  },
  exits: {
    serverError: {
      responseType: 'serverError'
    },
    badRequest: {
      responseType: 'badRequest'
    },
    success: {
      responseType: 'ok'
    }
  },
  fn: async function (inputs, exits) {
    if (!this.req.isSocket) {
      return exits.badRequest(new Error('This is reserved to socket only'))
    }
    try {
      // prevent streamer of having more than one simultanious active play
      const channel = await Channel.findOne({ id: this.req.user })
      const activePlays = await Play.find({channel: channel.id, status: 'in progress'})
      const playsPromises = activePlays.map(activePlay => Play.update({id: activePlay.id}).set({status: 'expired'}))
      await Promise.all(playsPromises)
      // create a new play and let viewers know about it
      const play = await Play.create({channel: channel.id, status: 'in progress', startHand: inputs.hand}).fetch()
      sails.sockets.broadcast(channel.id, 'newPlay', play, this.req)
      return exits.success(play)
    } catch (e) {
      return exits.serverError(e)
    }
  }
}
