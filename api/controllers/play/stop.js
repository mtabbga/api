module.exports = {
  friendlyName: 'Start',
  description: 'Start a prediction play.',
  inputs: {
    play: {
      type: 'string',
      description: 'The current active play id to predict on.',
      required: true
    },
    hand: {
      type: ['string'],
      description: 'The deck hand at the start of the play',
      required: true
    }
  },
  exits: {
    serverError: {
      responseType: 'serverError'
    },
    badRequest: {
      responseType: 'badRequest'
    },
    notFound: {
      responseType: 'notFound'
    },
    gone: {
      responseType: 'gone'
    },
    success: {
      responseType: 'ok'
    }
  },
  fn: async function (inputs, exits) {
    if (!this.req.isSocket) {
      return exits.badRequest(new Error('This is reserved to socket only'))
    }
    try {
      const play = await Play.findOne({ id: inputs.play})
      if (!play) {
        return exits.notFound(new Error('Can not find the play that the streamer is trying to finish'))
      }
      if (play.status !== 'in progress') {
        return exits.gone(new Error('The play that the steamer is trying to stop has expired or finished'))
      }
      const predictions = await Prediction.find({ play: play.id})
      const predictionsResults = predictions
        .map(async (prediction) => await Prediction
          .updateOne({id: prediction.id})
          .set({result: sails.helpers.calculateResult(play.startHand, inputs.hand, prediction.attempt)})
        )
      // const predictionsResults = await Promise.all(predictionsPromises)
      await Play.updateOne({ id: inputs.play}).set({status: 'done', endHand: inputs.hand, endedAt: new Date().valueOf()})
      const topTen = await Prediction.topTen(play.channel)
      sails.sockets.broadcast(play.channel, 'playStopped', {predictionsResults, topTen}, this.req)
      return exits.success({predictionsResults, topTen})
    } catch (e) {
      return exits.serverError(e)
    }
  }
}
