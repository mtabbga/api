/**
 * ChannelController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  async score(req, res) {
    try {
      const score = await Prediction.score({ viewer: req.query.viewer, channel: req.query.channel })
      return res.json({score})
    } catch (e) {
      return res.serverError(e)
    }
  },
  async topten(req, res) {
    try {
      const topTen = await Prediction.topTen(req.query.channel)
      return res.json(topTen)
    } catch (e) {
      return res.serverError(e)
    }
  }
}
