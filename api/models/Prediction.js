/**
 * Prediction.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

const status = require('../utils/status')

module.exports = {
  attributes: {
    attempt: {
      type: 'json',
      defaultsTo: null
    },
    status: {
      type: 'string',
      isIn: status
    },
    result: {
      type: 'number',
      defaultsTo: 0
    },
    // associations
    channel: {
      model: 'channel',
      required: true
    },
    play: {
      model: 'play',
      required: true
    },
    viewer: {
      model: 'viewer',
      required: true
    }
  },
  async score({ viewer, channel }) {
    const predictions = await Prediction.find({ viewer, channel })
    return predictions.reduce((acc, curr) => acc + curr.result, 0)
  },
  async topTen(channel) {
    const predictions = await Prediction.find({ channel })
    const mapReduce = predictions.reduce((acc, curr) => {
      if (!acc[curr.viewer]) {
        acc[curr.viewer] = { viewer: curr.viewer, result: 0 }
      }
      acc[curr.viewer].result += curr.result
      return acc
    }, {})
    return Object.values(mapReduce).sort((a, b) => a.result > b.result ? -1 : 1)
  }
}
