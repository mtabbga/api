/**
 * Viewer.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  dontUseObjectIds: true,
  attributes: {
    // associations
    predictions: {
      collection: 'prediction',
      via: 'viewer'
    },
    plays: {
      collection: 'play',
      through: 'prediction',
      via: 'viewer'
    }
  }
}
