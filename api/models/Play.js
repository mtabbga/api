/**
 * Play.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

const status = require('../utils/status')

module.exports = {
  attributes: {
    status: {
      type: 'string',
      isIn: status
    },
    endedAt: {
      type: 'number',
      allowNull: true
    },
    startHand: {
      type: 'json',
      defaultsTo: []
    },
    endHand: {
      type: 'json',
      defaultsTo: []
    },
    // associations
    channel: {
      model: 'channel'
    },
    predictions: {
      collection: 'prediction',
      via: 'play'
    },
    viewers: {
      collection: 'viewer',
      through: 'prediction',
      via: 'play'
    }
  }
}
