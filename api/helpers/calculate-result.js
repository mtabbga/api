module.exports = {
  friendlyName: 'Calculate result',
  description: 'Calculate result of a prediction at the end of a play',
  inputs: {
    start: {
      type: ['string'],
      description: 'hand at the start of the play',
      required: true
    },
    end: {
      type: ['string'],
      description: 'hand at the end of the play',
      required: true
    },
    guess: {
      type: ['string'],
      description: 'The prediction attempt',
      required: true
    }
  },
  exits: {
    success: {
      description: 'All done.',
    }
  },
  fn(inputs, exits) {
    const played = inputs.start.filter(card => inputs.end.indexOf(card) === -1)
    let score = inputs.guess.reduce((acc, curr) => acc + (played.indexOf(curr) + 1 ? 1 : -1), 0)
    return exits.success((score + Math.abs(score)) / 2)
  }
}
